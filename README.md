# ElixirCodeMe

## Help
https://github.com/StephenGrider/ElixirCode

Create project
```
mix new cards
```

Enter shell
```
iex -S mix
```

Run function
```
iex -S mix
Cards.hello

recompile
```

Create documentation
```
abrir mix.exs
dentro da lista deps adicionar:
{:ex_doc, "~> 0.27"}
mix deps.get
```

```
Adicionar @moduledoc dentro do módulo criado
mix docs
```

```
Adicionar @doc dentro do módulo criado acima de cada função
mix docs
```

Run tests
```
mix test
```
